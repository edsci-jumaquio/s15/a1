console.log('Hello, World')
// Details
const details = {
    fName: "Glenndale Yhuan",
    lName: "Jumaquio",
    age: 17,
    hobbies : [
        "drawing", "reading", "playing games"
    ] ,
    workAddress: {
        housenumber: "Block 14 Lot 35",
        street: "Jasmin Street",
        city: "Meycauayan City",
        state: "Bulacan, Philippines",
    }
}
const work = Object.values(details.workAddress);
console.log("My First Name is " + details.fName)
console.log("My Last Name is " + details.lName)
console.log(`Yes, I am ${details.fName} ${details.lName}.`)
console.log("I am " + details.age + " years old.")
console.log(`My hobbies are ${details.hobbies.join(', ')}.`);
console.log("I work at " + work.join(", ") + ".");